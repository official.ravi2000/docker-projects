package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao {
	private final Logger LOGGER = LoggerFactory.getLogger(UserDao.class);

	public void save() {
		LOGGER.debug("*******save method execution Start");
		// logic to save record in DB
		System.out.println("UserDao.save()");
		
		LOGGER.info("*******Record inserted successfully");

		LOGGER.debug("*******save method execution Completed");
	}
}
